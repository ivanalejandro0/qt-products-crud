#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Product(object):
    def __init__(self):
        self.name = ""
        self.detail = ""
        self.price = 0.0

    def __str__(self):
        return "Name: %s; Detail: %s; Price: %s." % (
            self.name, self.detail, self.price)
