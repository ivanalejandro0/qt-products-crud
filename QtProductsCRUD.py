#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

try:
    from PyQt4 import QtCore, QtGui, uic
except ImportError:
    print "PyQt4 (Qt4 bindings for Python) is required for this application."
    print "You can find it here: http://www.riverbankcomputing.co.uk"
    sys.exit()

from AddProduct import AddProduct
from ProductsList import ProductsList


class QtProductsCRUD(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)

        self.initGUI()

    def initGUI(self):
        uifile = os.path.join(
            os.path.abspath(os.path.dirname(__file__)), 'qt_products_crud.ui')

        uic.loadUi(uifile, self)
        self.ui = self

        # Add listing
        layout1 = QtGui.QGridLayout()
        widget1 = self.twContainer.widget(0)

        ProductsListWidget = ProductsList(widget1)
        layout1.addWidget(ProductsListWidget, 0, 0)
        widget1.setLayout(layout1)

        # Add inserting
        layout2 = QtGui.QGridLayout()
        widget2 = self.twContainer.widget(1)
        AddProductWidget = AddProduct(widget2)

        layout2.addWidget(AddProductWidget, 0, 0, 1, 1, QtCore.Qt.AlignTop)
        widget2.setLayout(layout2)

        self.connect(ProductsListWidget, QtCore.SIGNAL("statusChanged"), self.updateStatus)

    def updateStatus(self, st):
        self.statusBar().showMessage(st)


def main():
    app = QtGui.QApplication(sys.argv)
    prog = QtProductsCRUD()
    prog.show()
    app.exec_()

    return 0

if __name__ == '__main__':
    main()
