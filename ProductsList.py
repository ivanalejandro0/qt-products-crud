#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

try:
    from PyQt4 import QtCore, QtGui, uic
except ImportError:
    print "PyQt4 (Qt4 bindings for Python) is required for this application."
    print "You can find it here: http://www.riverbankcomputing.co.uk"
    sys.exit()

from ProductsModel import ProductsModel


class ProductsList(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.initGUI()

        self.products = ProductsModel('data/products.db')

        self.listLimit = 50
        self.listOffset = 0
        self.products.set_limit(self.listLimit, self.listOffset)
        self._reFillTable()

    def _addColumns(self, cols):
        n = 0

        for i in xrange(len(cols)):
            self.twListing.insertColumn(0)

        for col in cols:
            column = QtGui.QTableWidgetItem(self.tr(col))
            self.twListing.setHorizontalHeaderItem(n, column)
            n = n + 1

    def initGUI(self):
        uifile = os.path.join(
            os.path.abspath(os.path.dirname(__file__)), 'Listing.ui')

        uic.loadUi(uifile, self)
        self.ui = self

        columns = ['Name', 'Detail', 'Price']
        self._addColumns(columns)

        self.twListing.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)

        self.setWindowTitle(self.tr('Product listing'))

    def _reFillTable(self):
        # empty table
        for r in xrange(self.twListing.rowCount()):
            self.twListing.removeRow(0)

        # Add products
        count = self.products.count_query_result()
        for prod in self.products.get_items():
            row = self.twListing.rowCount()
            self.twListing.insertRow(row)
            self.twListing.setItem(row, 0, QtGui.QTableWidgetItem(prod.name))
            self.twListing.setItem(row, 1, QtGui.QTableWidgetItem(prod.detail))
            self.twListing.setItem(row, 2, QtGui.QTableWidgetItem(str(prod.price)))

        pages = count/self.listLimit + 1

        self.setStatus('Showing %s of %s products. Page %s of %s' % (
            (count, self.products.count_items(), self.listOffset + 1, pages)))

        # Enable or disable the Next button
        if pages > 1 and self.listOffset+1 < pages:
            nextEnabled = True
        else:
            nextEnabled = False
        self.pbNext.setEnabled(nextEnabled)

        # Enable or disable the Previous button
        if self.listOffset > 0:
            prevEnabled = True
        else:
            prevEnabled = False
        self.pbPrevious.setEnabled(prevEnabled)

    def on_leSearchTerm_textEdited(self, text):
        text = str(text)
        self.listOffset = 0
        self.products.set_filter(text, text)
        self.products.set_limit(self.listLimit, self.listOffset)

        self._reFillTable()

    def on_twListado_itemSelectionChanged(self):
        row = self.twListing.selectedItems()
        if row:
            enabled = True
        else:
            enabled = False

        self.pbDelete.setEnabled(enabled)

    @QtCore.pyqtSlot()
    def on_pbNext_clicked(self):
        self.listOffset += 1
        self.products.set_limit(self.listLimit, self.listOffset)
        self._reFillTable()

    @QtCore.pyqtSlot()
    def on_pbPrevious_clicked(self):
        self.listOffset -= 1
        self.products.set_limit(self.listLimit, self.listOffset)
        self._reFillTable()

    @QtCore.pyqtSlot()
    def on_pbDelete_clicked(self):
        # ask confirmation before delete
        res = QtGui.QMessageBox.question(
            None, 'Delete row',
            'Are you sure that you want to delete the selected row?',
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if res == QtGui.QMessageBox.Yes:
            fila = self.twListing.selectedItems()
            nameItem = fila[0]
            name = nameItem.data(0).toString()

            self.products.remove_items(name)
            self._reFillTable()

    def setStatus(self, st):
        ''' Set status in a label if is a stand alone widget or emits
            a signal if composes another widget
        '''
        if self.parent() is None:
            self.tlStatus.setText(st)
        else:
            self.emit(QtCore.SIGNAL("statusChanged"), st)


def main():
    app = QtGui.QApplication(sys.argv)
    prog = ProductsList()
    prog.show()
    app.exec_()

    return 0

if __name__ == '__main__':
    main()
