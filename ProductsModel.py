#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3 as dbapi
from Product import Product


class ProductsModel(object):
    def __init__(self, dbname=':memory:', init=False):
        self._database = dbapi.connect(dbname)
        self._cursor = self._database.cursor()

        if dbname == ':memory:' or init:
            self._initDB()

        self.query_filter = ""
        self.query_limit = ""
        self.order = " ORDER BY name"

    def __del__(self):
        self._cursor.close()
        self._database.close()

    def _initDB(self):
        self._cursor.execute("""
            CREATE TABLE products (name TEXT PRIMARY KEY,
            detail TEXT, price FLOAT(2))
            """)

        self._database.commit()

    def _make_filter_conditions(self, column, line):
        """ Creates SQL filter conditions

        :column: a column in the table, like 'name'
        :line: a line like 'keyboard   genius\n black\twhite   '
        :returns: ((name LIKE '%keyboard%') AND (name LIKE '%genius%') AND \
                   (name LIKE '%black%') AND (name LIKE '%white%'))
        """
        line = line.strip()
        line = line.replace('\t', ' ')
        line = line.replace('\n', ' ')
        terms = line.split(' ')

        conds = "("

        for t in terms:
            if t != '':
                conds += "(" + column + " LIKE '%" + t + "%')"
                if t != terms[-1]:
                    conds += " AND "

        conds += ')'
        return conds

    def set_filter(self, name="", detail=""):
        self.query_filter = ""
        if name or detail:
            fn = self._make_filter_conditions('name', name)
            fd = self._make_filter_conditions('detail', detail)
            self.query_filter = (" WHERE " + fn + " OR " + fd)

    def set_limit(self, items_limit=None, offset=None):
        self.query_limit = ''

        if items_limit is not None:
            self.query_limit = " LIMIT " + str(items_limit)
            if offset is not None:
                self.query_limit = (self.query_limit + ' offset ' +
                                    str(offset * items_limit))

    def count_query_result(self):
        ''' Count the results for the actual configuration of the query '''
        query = "SELECT COUNT(*) FROM products" + self.query_filter
        self._cursor.execute(query)

        return self._cursor.fetchone()[0]

    def get_items(self):
        ''' Generator with items from db '''
        self.count_query_result()

        query = ("SELECT * FROM products" +
                 self.query_filter + self.order + self.query_limit)

        self._cursor.execute(query)

        for item in self._cursor:
            prod = Product()
            prod.name = item[0]
            prod.detail = item[1]
            prod.price = item[2]
            yield prod

    def count_items(self):
        ''' Return count of items in db '''
        self._cursor.execute('SELECT COUNT(*) FROM products')
        return self._cursor.fetchone()[0]

    def save_item(self, prod):
        ''' Inserts a new product in the db. '''
        try:
            query = "INSERT INTO products VALUES (?, ?, ?)"
            arguments = (prod.name, prod.detail, prod.price)
            self._cursor.execute(query, arguments)
            self._database.commit()
        except dbapi.IntegrityError:
            print "Integrity Error: repeated entry '%s'." % (prod.name, )

    def update_item(self, prod):
        ''' Updates the product's data, or creates it if doesn't exists. '''
        try:
            query = "INSERT INTO products VALUES (?, ?, ?)"
            self._cursor.execute(query, prod.name, prod.detail, prod.price)
        except dbapi.IntegrityError:
            query = "UPDATE products SET name=?, detail=?, price=? WHERE name=?"

            arguments = (prod.name, prod.detail, prod.price, prod.name)
            self._cursor.execute(query, arguments)

        self._database.commit()

    def remove_items(self, criteria):
        query = "DELETE FROM products WHERE name='%s'"
        self._cursor.execute(query % (criteria, ))
        self._database.commit()
        print "%s products deleted." % self._database.total_changes
