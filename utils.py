#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Product import Product
from ProductsModel import ProductsModel


def text_to_product(txt):
    ''' Converts a line like: Name\tDetail\tPrice
        in a "Product" object.'''
    prod = txt.split('\t')

    if len(prod) == 3:
        product = Product()
        product.name = prod[0]
        product.detail = prod[1]
        product.price = prod[2]
    else:
        # if line is invalid
        product = None

    return product


def insert_from_file(model, file_name):
    for line in open(file_name):
        prod = text_to_product(line)
        if prod:
            model.save_item(prod)


def main():
    pm = ProductsModel('data/products.db', init=True)
    insert_from_file(pm, 'data/demo.csv')
    return 0

if __name__ == '__main__':
    main()
