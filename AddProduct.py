#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

try:
    from PyQt4 import QtCore, QtGui, uic
except ImportError:
    print "PyQt4 (Qt4 bindings for Python) is required for this application."
    print "You can find it here: http://www.riverbankcomputing.co.uk"
    sys.exit()

from Product import Product
from ProductsModel import ProductsModel


class AddProduct(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.initGUI()

        self.products = ProductsModel('data/products.db')

    def initGUI(self):
        uifile = os.path.join(
            os.path.abspath(os.path.dirname(__file__)), 'AddProduct.ui')

        uic.loadUi(uifile, self)
        self.ui = self

        self.setWindowTitle(self.tr('Add Product'))

        pricesValidator = QtGui.QDoubleValidator(self.lePrice)
        pricesValidator.setBottom(0)  # no negatives
        pricesValidator.setDecimals(2)
        pricesValidator.setNotation(QtGui.QDoubleValidator.StandardNotation)
        self.lePrice.setValidator(pricesValidator)

    @QtCore.pyqtSlot()
    def on_pbSave_clicked(self):
        prod = Product()

        prod.name = str(self.leName.text())
        prod.detail = str(self.leDetail.text())
        prod.price = str(self.lePrice.text())

        self.products.save_item(prod)

    @QtCore.pyqtSlot()
    def on_pbCancel_clicked(self):
        self.leName.setText('')
        self.leDetail.setText('')
        self.lePrice.setText('')


def main():
    app = QtGui.QApplication(sys.argv)
    prog = AddProduct()
    prog.show()
    app.exec_()

    return 0

if __name__ == '__main__':
    main()
